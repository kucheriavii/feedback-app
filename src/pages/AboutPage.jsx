import React from 'react'
import { Link } from 'react-router-dom'
import Card from '../components/shared/Card'

export default function AboutPage() {
  return (
    <Card>
        <div className="about">
            <h1>About this project</h1>
            <p>
                This is a React app to leave comment for a product or a service
            </p>
            <p>version: 1.0.0</p>
            <p>
                <Link to='/'>Back To Home</Link>
            </p>
        </div>
    </Card>
  )
}
