const FeedbackData = [
    {
      id: 1,
      rating:10,
      text: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Officiis pariatur explicabo eaque nobis sed perspiciatis aspernatur ullam non, fuga placeat nemo laborum perferendis, quaerat rerum! Ut quisquam qui laborum ipsam.'
    },
    {
      id: 2,
      rating:9,
      text: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Officiis pariatur explicabo eaque nobis sed perspiciatis aspernatur ullam non, fuga placeat nemo laborum perferendis, quaerat rerum! Ut quisquam qui laborum ipsam.'
    },
    {
      id: 3,
      rating:8,
      text: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Officiis pariatur explicabo eaque nobis sed perspiciatis aspernatur ullam non, fuga placeat nemo laborum perferendis, quaerat rerum! Ut quisquam qui laborum ipsam.'
    },
  ]

  export default FeedbackData;