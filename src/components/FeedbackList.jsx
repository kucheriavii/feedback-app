import React from 'react'
import FeedbackItem from './FeedbackItem'
import { AnimatePresence, motion } from 'framer-motion';
import { useContext } from 'react';
import FeedbackContext from '../context/FeedbackContext';
import Spiner from './shared/Spiner';

export default function FeedbackList() {
    const {feedback, isLoading} = useContext(FeedbackContext);


    if (!isLoading && (!feedback || feedback.length === 0)){
        return <p>No feedback yet</p>
    }

    return isLoading ? <Spiner /> : (
        <AnimatePresence>
            <div className='feedback-list'>
                {feedback.map((item) => (
                    <motion.div 
                            key={item.id}
                            initial={{opacity:0}}
                            animate={{opacity:1}}
                            exit={{opacity:0.5}}
                            >
                        <FeedbackItem key={item.id} item={item} />
                    </motion.div>
                ))}
            </div>
        </AnimatePresence>
    )

    // return (
    //     <div className='feedback-list'>
    //         {feedback.map((item) => (
    //             <FeedbackItem key={item.id} item={item} handleDelete={handleDelete}/>
    //         ))}
    //     </div>
    // )
}

